# Programa para crear las Solicitudes Recurrentes
## Objetivo
- Se crea este pequeño programa para que dado un archivo .csv de la solicitud recurrente nos permita crear el body de las solicitudes.
## Configuración
- Cada archivo que se requiera ejecutar deberá ser guardado en la carpeta src > main > resources
- En la carpeta src > main > resources > examples se encuentran unos ejemplos de los archivos que deben estar en resources
- Este proyecto esta creado con Maven usando Java 17.
### Requests Recurring W2A
En el menu de la consola seleccionar 1
1. **Solicitud W2A** <===
2. Solicitud A2W
3. Solicitud Change Network

Para ejecutar una solicitud W2A se debe subir un archivo .csv, con las siguientes columnas y en ese orden: **idSeller, idNewSeller, idSellerNetwork, dateExecution**
- idSeller: ID del Weliker que se va a convertir a Amigo.
- idNewSeller: ID del Weliker padre del nuevo Amigo.
- idSellerNetwork: ID de la Red del Weliker.
- dateExecution: Fecha de ejecución del cronjob (siempre debe ser superior a la fecha actual al menos por 1 día).  El formato de esta fecha es dia/mes/año > dd/MM/yyyy (ejm 6/2/2024)

***Nota: El nombre del archivo a subir debe ser "Weliker_Friend.csv" y el archivo de salida después de ejecutar el programa es "weliker2friend_Formatted.txt"*** 


### Requests Recurring A2W
En el menu de la consola seleccionar 2
1. Solicitud W2A
2. **Solicitud A2W** <===
3. Solicitud Change Network

Para ejecutar una solicitud W2A se debe subir un archivo .csv, con las siguientes columnas y en ese orden: **idShopper,welikerCode,dateExecution,acceptanceTerms,acceptanceTratment,acceptanceWeLikeContract**
- idShopper: ID del Amigo que se convertirá en Weliker.
- welikerCode: Código único como nuevo Weliker
- dateExecution: Fecha en la que se ejecutará la solicitud (Esta puede ser de la fecha actual o futura).  El formato de esta fecha es dia/mes/año > dd/MM/yyyy (ejm 6/2/2024)
- acceptanceTerms: Este campo irá en 1 si en la tabla polices del schema user existe esta politica, en caso contrario debe setearse en 0
- acceptanceTratment: Este campo irá en 1 si en la tabla polices del schema user existe esta politica, en caso contrario debe setearse en 0
- acceptanceWeLikeContract: Este campo irá en 1 si en la tabla polices del schema user existe esta politica, en caso contrario debe setearse en 0

***Nota: El nombre del archivo a subir debe ser "Friend_Weliker.csv" y el archivo de salida después de ejecutar el programa es "friend2weliker_Formatted.txt"***

### Requests Recurring Change Network
En el menu de la consola seleccionar 3
1. Solicitud W2A
2. Solicitud A2W
3. **Solicitud Change Network** <===

Para ejecutar una solicitud W2A se debe subir un archivo .csv, con las siguientes columnas y en ese orden: **idShopper,idNewSeller,dateExecution**
- idShopper: Id del Amigo que quiere cambiarse de red
- idNewSeller: Id del Weliker al que va a quedar asociado el Amigo
- dateExecution: Fecha de ejecución del cronjob (siempre debe ser superior a la fecha actual al menos por 1 día). El formato de esta fecha es dia/mes/año > dd/MM/yyyy (ejm 6/2/2024)

***Nota: El nombre del archivo a subir debe ser "Change_Network.csv" y el archivo de salida después de ejecutar el programa es "network_change_Formatted.txt"***