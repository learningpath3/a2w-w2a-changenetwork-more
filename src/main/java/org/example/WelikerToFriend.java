package org.example;

public class WelikerToFriend {
    private String idSeller;
    private String idNewSeller;
    private String idSellerNetwork;
    private String dateExecution;

    public WelikerToFriend(String idSeller, String idNewSeller, String idSellerNetwork, String dateExecution) {
        this.idSeller = idSeller;
        this.idNewSeller = idNewSeller;
        this.idSellerNetwork = idSellerNetwork;
        this.dateExecution = dateExecution;
    }

    public String getIdSeller() {
        return idSeller;
    }

    public void setIdSeller(String idSeller) {
        this.idSeller = idSeller;
    }

    public String getIdNewSeller() {
        return idNewSeller;
    }

    public void setIdNewSeller(String idNewSeller) {
        this.idNewSeller = idNewSeller;
    }

    public String getIdSellerNetwork() {
        return idSellerNetwork;
    }

    public void setIdSellerNetwork(String idSellerNetwork) {
        this.idSellerNetwork = idSellerNetwork;
    }

    public String getDateExecution() {
        return dateExecution;
    }

    public void setDateExecution(String dateExecution) {
        this.dateExecution = dateExecution;
    }

    @Override
    public String toString() {
        return  "{\n"+
                    "\t\"idSeller\":" + idSeller + ",\n" +
                    "\t\"idNewSeller\":" + idNewSeller + ",\n" +
                    "\t\"idSellerNetwork\":" + idSellerNetwork + ",\n" +
                    "\t\"dateExecution\": \"" + this.dateExecution + "\"\n" +
                "}";
    }
}
