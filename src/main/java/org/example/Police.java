package org.example;

public enum Police {


    ACCEPTANCETERMS("acceptanceTerms", "1.0"),
    ACCEPTANCETRATMENT("acceptanceTratment", "1.0"),
    ACCEPTANCEWELIKECONTRACT("acceptanceWeLikeContract", "1.0");

    private final String policeName;
    private final String policeVersion;

    Police(String policeName, String policeVersion) {
        this.policeName = policeName;
        this.policeVersion = policeVersion;
    }

    public String getPoliceName() {
        return policeName;
    }

    public String getPoliceVersion() {
        return policeVersion;
    }
}