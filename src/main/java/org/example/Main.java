package org.example;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        menu();
    }

    public static void menu(){
        Scanner scanner = new Scanner(System.in);
        boolean isValid = false;

        do {

            System.out.println("Seleccione Que opción quiere ejecutar: \n" +
                    "1. Solicitud W2A\n" +
                    "2. Solicitud A2W\n" +
                    "3. Solicitud Change Network\n>>>");
            String option = scanner.next();
            switch (option) {
                case "1":
                    uploadFileWelikerToFriend();
                    isValid = true;
                    break;
                case "2":
                    uploadFileFriendToWeliker();
                    isValid = true;
                    break;
                case "3":
                    uploadFileChangeNetwork();
                    isValid = true;
                    break;
                default:
                    System.err.println("Seleccione una opción correcta");
                    break;

            }
        }while (!isValid);
    }
    public static String uploadFileChangeNetwork(){
        try {
            // Lee el contenido del archivo JSON
                String recurrentReq = "src/main/resources/Change_Network.csv";
            System.out.println(recurrentReq);
            CSVReader recurrentReqReader = new CSVReader(new FileReader(recurrentReq));

            List<ChangeNetwork> recurrentReqList = createChangeNetworkList(recurrentReqReader);

            List<String> requestBody = new ArrayList<>();
            Long count = 0L;
            for (ChangeNetwork changeNetwork : recurrentReqList) {
                requestBody.add(changeNetwork.toString());
            }

            requestBody.forEach(System.out::println);
            createFileTxt(requestBody,"network_change_Formatted");
            return "";//outputJson;
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return "";
        } catch (CsvValidationException e) {
            throw new RuntimeException(e);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public static String uploadFileWelikerToFriend(){
        try {
            // Lee el contenido del archivo JSON
            String recurrentReq = "src/main/resources/Weliker_Friend.csv";
            System.out.println(recurrentReq);
            CSVReader recurrentReqReader = new CSVReader(new FileReader(recurrentReq));

            List<WelikerToFriend> recurrentReqList = createWelikerToFriendList(recurrentReqReader);

            List<String> requestBody = new ArrayList<>();
            Long count = 0L;
            for (WelikerToFriend welikerToFriend : recurrentReqList) {
                requestBody.add(welikerToFriend.toString());
            }

            requestBody.forEach(System.out::println);
            createFileTxt(requestBody,"weliker2friend_Formatted");
            return "";//outputJson;
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return "";
        } catch (CsvValidationException e) {
            throw new RuntimeException(e);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public static String uploadFileFriendToWeliker(){
        try {
            // Lee el contenido del archivo JSON
            String recurrentReq = "src/main/resources/Friend_Weliker.csv";
            System.out.println(recurrentReq);
            CSVReader recurrentReqReader = new CSVReader(new FileReader(recurrentReq));

            List<FriendToWeliker> recurrentReqList = createFriendToWelikerList(recurrentReqReader);

            List<String> requestBody = new ArrayList<>();
            StringBuilder UpdatesUser = new StringBuilder();
            Long count = 0L;
            for (FriendToWeliker friendToWeliker : recurrentReqList) {
                requestBody.add(friendToWeliker.toString());
                UpdatesUser.append(friendToWeliker.getUpdateUser());
            }

            requestBody.add(UpdatesUser.toString());
            requestBody.forEach(System.out::println);
            createFileTxt(requestBody,"friend2weliker_Formatted");
            return "";//outputJson;
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return "";
        } catch (CsvValidationException e) {
            throw new RuntimeException(e);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    private static List<ChangeNetwork> createChangeNetworkList(CSVReader reader) throws CsvValidationException, IOException, ParseException {
        String[] nextLine;

        // Crear formateador para el formato de fecha y hora
        SimpleDateFormat formatoOriginal = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatoDeseado = new SimpleDateFormat("yyyy-MM-dd");

        List<ChangeNetwork> auditLogList = new ArrayList<>();
        reader.readNext();//Para evitar el Encabezado
        while ((nextLine = reader.readNext()) != null) {
            // nextLine[] contiene los valores de la línea actual
            Date date = formatoOriginal.parse(nextLine[2]);
            ChangeNetwork auditLog = new ChangeNetwork(nextLine[0],nextLine[1],formatoDeseado.format(date));
            auditLogList.add(auditLog);
        }
        return auditLogList;
    }
    private static List<WelikerToFriend> createWelikerToFriendList(CSVReader reader) throws CsvValidationException, IOException, ParseException {
        String[] nextLine;

        // Crear formateador para el formato de fecha y hora
        SimpleDateFormat formatoOriginal = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatoDeseado = new SimpleDateFormat("yyyy-MM-dd");

        List<WelikerToFriend> welikerToFriendList = new ArrayList<>();
        reader.readNext();//Para evitar el Encabezado
        while ((nextLine = reader.readNext()) != null) {
            // nextLine[] contiene los valores de la línea actual
            Date date = formatoOriginal.parse(nextLine[3]);
            WelikerToFriend welikerToFriend = new WelikerToFriend(nextLine[0],nextLine[1],nextLine[2], formatoDeseado.format(date));
            welikerToFriendList.add(welikerToFriend);
        }
        return welikerToFriendList;
    }

    private static List<FriendToWeliker> createFriendToWelikerList(CSVReader reader) throws CsvValidationException, IOException, ParseException {
        String[] nextLine;

        // Crear formateador para el formato de fecha y hora
        SimpleDateFormat formatoOriginal = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatoDeseado = new SimpleDateFormat("yyyy-MM-dd");

        List<FriendToWeliker> friendToWelikersList = new ArrayList<>();
        reader.readNext();//Para evitar el Encabezado
        while ((nextLine = reader.readNext()) != null) {
            // nextLine[] contiene los valores de la línea actual
            Date date = formatoOriginal.parse(nextLine[2]);
            FriendToWeliker friendToWeliker = new FriendToWeliker(nextLine[0],nextLine[1],formatoDeseado.format(date), new ArrayList<>());
            if(!nextLine[3].equals("1"))
                friendToWeliker.addPolice(Police.ACCEPTANCETERMS);
            if(!nextLine[4].equals("1"))
                friendToWeliker.addPolice(Police.ACCEPTANCETRATMENT);
            if(!nextLine[5].equals("1"))
                friendToWeliker.addPolice(Police.ACCEPTANCEWELIKECONTRACT);

            friendToWelikersList.add(friendToWeliker);
        }
        return friendToWelikersList;
    }


    private static void createFileTxt(List<String> sentencesSql, String nameFile){
        String path= "src/main/resources/"+nameFile+".txt";
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
            // Escribir cada cadena en una nueva línea en el archivo
            for (String sentence : sentencesSql) {
                writer.write(sentence);
                writer.newLine();  // Agregar una nueva línea después de cada cadena
            }
            System.out.println("Archivo creado con éxito.");
        } catch (IOException e) {
            System.out.println("Error al crear el archivo: " + e.getMessage());
        }
    }
}

