package org.example;

public class ChangeNetwork {
    private String idShopper;
    private String idNewSeller;
    private String dateExecution;

    public ChangeNetwork(String idShopper, String idNewSeller, String dateExecution) {
        this.idShopper = idShopper;
        this.idNewSeller = idNewSeller;
        this.dateExecution = dateExecution;
    }

    public String getIdShopper() {
        return idShopper;
    }

    public void setIdShopper(String idShopper) {
        this.idShopper = idShopper;
    }

    public String getIdNewSeller() {
        return idNewSeller;
    }

    public void setIdNewSeller(String idNewSeller) {
        this.idNewSeller = idNewSeller;
    }

    public String getDateExecution() {
        return dateExecution;
    }

    public void setDateExecution(String dateExecution) {
        this.dateExecution = dateExecution;
    }

    @Override
    public String toString() {
        return  "{\n" +
                    "\t\"idShopper\":"+idShopper+",\n" +
                    "\t\"idNewSeller\":"+idNewSeller+",\n"+
                    "\t\"dateExecution\": \""+dateExecution +"\"\n"+
                "}";

    }
}
