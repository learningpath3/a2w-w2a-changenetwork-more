package org.example;

import java.util.List;

public class FriendToWeliker {
    private String idShopper;
    private String welikerCode;
    private String dateExecution;
    private List<Police> polices;

    public FriendToWeliker(String idShopper, String welikerCode, String dateExecution, List<Police> polices) {
        this.idShopper = idShopper;
        this.welikerCode = welikerCode;
        this.dateExecution = dateExecution;
        this.polices = polices;
    }

    public String getIdShopper() {
        return idShopper;
    }

    public void setIdShopper(String idShopper) {
        this.idShopper = idShopper;
    }

    public String getWelikerCode() {
        return welikerCode;
    }

    public void setWelikerCode(String welikerCode) {
        this.welikerCode = welikerCode;
    }

    public String getDateExecution() {
        return dateExecution;
    }

    public void setDateExecution(String dateExecution) {
        this.dateExecution = dateExecution;
    }

    public List<Police> getPolices() {
        return polices;
    }

    public void setPolices(List<Police> polices) {
        this.polices = polices;
    }
    public boolean addPolice(Police police){
        return this.polices.add(police);
    }

    public String getUpdateUser(){
        return "update \"user\".users set level_id = 1 where id = " + this.idShopper + ";\n";
    }

    @Override
    public String toString() {
        String policesString = "";
        for (Police police : this.polices){
            policesString +=
                    "\t\t{\n" +
                    "\t\t\t\"policeName\":\""+police.getPoliceName()+"\",\n" +
                    "\t\t\t\"policeVersion\":\""+police.getPoliceVersion()+"\"\n" +
                    "\t\t}\n";
        }

        return "{\n" +
                "\t\"idShopper\":"+this.idShopper+",\n" +
                "\t\"welikerCode\":\""+this.welikerCode+"\",\n" +
                "\t\"dateExecution\": \""+this.dateExecution+"\",\n" +
                "\t\"polices\":[\n" +
                policesString +
                "\t]\n" +
                "}";
    }
}
